import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KnowledgeBaseComponent } from './knowledge-base.component';
import { KnowledgeBaseRoutingModule } from './knowledge-base-routing.module';



@NgModule({
  declarations: [KnowledgeBaseComponent],
  imports: [
    CommonModule,
    KnowledgeBaseRoutingModule
  ]
})
export class KnowledgeBaseModule { }
