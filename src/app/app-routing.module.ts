import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StatusResolver } from './core/resolver/status.resolver';
import { ExecutorResolver } from './core/resolver/executor.resolver';


const routes: Routes = [
  {path: 'knowledge', loadChildren: () => import('./knowledge-base/knowledge-base.module').then(m => m.KnowledgeBaseModule)},
  {
    path: 'request',
    loadChildren: () => import('./request/request.module').then(m => m.RequestModule),
    resolve: {
      statusList: StatusResolver,
      executorList: ExecutorResolver
    }
  },
  {path: 'employee', loadChildren: () => import('./employee/employee.module').then(m => m.EmployeeModule)},
  {path: 'customer', loadChildren: () => import('./customer/customer.module').then(m => m.CustomerModule)},
  {path: 'assets', loadChildren: () => import('./assets/assets.module').then(m => m.AssetsModule)},
  {path: 'settings', loadChildren: () => import('./settings/settings.module').then(m => m.SettingsModule)},
  {path: '', pathMatch: 'full', redirectTo: 'request'},
  {path: '**', redirectTo: 'request'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
