import { Component, OnInit, OnDestroy, ComponentFactoryResolver, ViewChild, ViewContainerRef, ComponentRef } from '@angular/core';
import { TaskStore } from './service/task.service';
import { ITask } from '../core/model/task.interface';
import { Observable, Subject } from 'rxjs';
import { NewTaskFormComponent } from './component/new-task-form/new-task-form.component';
import { RefDirective } from '../core/directive/ref.directive';
import { EditTaskFormComponent } from './component/edit-task-form/edit-task-form.component';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.scss']
})
export class RequestComponent implements OnInit, OnDestroy {
  taskList$: Observable<ITask[]> = this.store.taskList;
  displayedColumns: string[] = ['ID', 'name', 'statusName', 'executorName'];
  @ViewChild(RefDirective, {static: false}) refDir: RefDirective;

  private destroyed$ = new Subject();

  constructor(private store: TaskStore,
              private resolver: ComponentFactoryResolver) { }

  ngOnInit() {
    this.store.updateState();
  }

  ngOnDestroy() {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  public addTask() {
    const component = this.componentFactory<NewTaskFormComponent>(NewTaskFormComponent);

    component.instance.close
    .pipe(
      takeUntil(this.destroyed$)
    )
    .subscribe( () => {
      this.refDir.containerRef.clear();
    });

    component.instance.createdTaskID
    .pipe(
      takeUntil(this.destroyed$)
    )
    .subscribe( (id: number) => {
      this.store.updateState();
      this.store.getTask(id).subscribe((response: ITask) => this.editTask(response));
    });
  }

  public editTask(task: ITask) {
    const component = this.componentFactory<EditTaskFormComponent>(EditTaskFormComponent);

    component.instance.close
    .pipe(
      takeUntil(this.destroyed$)
    )
    .subscribe( () => {
      this.refDir.containerRef.clear();
    });

    component.instance.task = task;
  }

  private componentFactory<C>(className: Type<C>): ComponentRef<C> {
    const component = this.resolver.resolveComponentFactory(className);
    this.refDir.containerRef.clear();
    return this.refDir.containerRef.createComponent(component);
  }
}

interface Type<T> extends Function {
  new (...args: any[]): T
}
