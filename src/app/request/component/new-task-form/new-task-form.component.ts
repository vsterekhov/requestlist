import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TaskStore } from '../../service/task.service';

@Component({
  selector: 'app-new-task-form',
  templateUrl: './new-task-form.component.html',
  styleUrls: ['./new-task-form.component.scss']
})
export class NewTaskFormComponent implements OnInit {
  @Output() close = new EventEmitter<void>();
  @Output() createdTaskID = new EventEmitter<number>();

  newTaskForm = new FormGroup({
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required)
  });

  constructor(private store: TaskStore) { }

  ngOnInit() {
  }

  onSubmit() {
    this.store.addTask(this.newTaskForm.value).subscribe(
      (response: number) => this.createdTaskID.emit(response)
    );
  }
}
