import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TaskStore } from '../../service/task.service';
import { ITask } from 'src/app/core/model/task.interface';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { IStatus } from 'src/app/core/model/status.interface';
import { ActivatedRoute } from '@angular/router';
import { IExecutor } from 'src/app/core/model/executor.interface';

@Component({
  selector: 'app-edit-task-form',
  templateUrl: './edit-task-form.component.html',
  styleUrls: ['./edit-task-form.component.scss']
})
export class EditTaskFormComponent implements OnInit {
  @Input() task: ITask;
  @Output() close = new EventEmitter<void>();
  public editTaskForm: FormGroup;
  public statusList: IStatus[];
  public executorList: IExecutor[];
  public statusColor: string;

  constructor(private store: TaskStore,
              private fb: FormBuilder,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.statusList = this.route.snapshot.data.statusList;
    this.statusColor = this.task.statusRgb;
    this.executorList = this.route.snapshot.data.executorList;

    this.editTaskForm = this.fb.group({
       comment: [''],
       statusId: [this.task.statusId, Validators.required],
       executorId: [this.task.executorId, Validators.required]
     });
  }

  onSubmit() {
    const data = Object.assign({}, this.editTaskForm.value, {id: this.task.id});

    this.store.changeTask(data).subscribe( () => {
      this.store.updateState();
      this.close.emit();
    });
  }

  changeStatus(event) {
    this.statusColor = this.statusList.find(item => item.id === event.value).rgb;
  }

}
