import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestComponent } from './request.component';
import { RequestRoutingModule } from './request-routing.module';
import { NewTaskFormComponent } from './component/new-task-form/new-task-form.component';
import { RefDirective } from '../core/directive/ref.directive';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { EditTaskFormComponent } from './component/edit-task-form/edit-task-form.component';
import { SanitizeHtmlPipe } from '../core/pipe/sanitize-html.pipe';
import { MatSelectModule } from '@angular/material/select';

@NgModule({
  declarations: [
    RequestComponent,
    NewTaskFormComponent,
    RefDirective,
    EditTaskFormComponent,
    SanitizeHtmlPipe
  ],
  imports: [
    CommonModule,
    RequestRoutingModule,
    ReactiveFormsModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule
  ],
  providers: [],
  entryComponents: [NewTaskFormComponent, EditTaskFormComponent]
})
export class RequestModule { }
