import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/core/service/api.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { ITask } from 'src/app/core/model/task.interface';
import { IStatus } from 'src/app/core/model/status.interface';
import { IExecutor } from 'src/app/core/model/executor.interface';

@Injectable({
  providedIn: 'root'
})
export class TaskStore {
  private _taskList: BehaviorSubject<ITask[]> = new BehaviorSubject([]);

  constructor(private api: ApiService) { }

  public get taskList() {
    return this._taskList.asObservable();
  }

  public updateState() {
    this.api.getAllTasks().subscribe((state: ITask[]) => this._taskList.next(state));
  }

  public addTask(data): Observable<number> {
    return this.api.addTask(data);
  }

  public getTask(id: number): Observable<ITask> {
    return this.api.getTask(id);
  }

  public getStatusList(): Observable<IStatus[]> {
    return this.api.getStatusList();
  }

  public getExecutorList(): Observable<IExecutor[]> {
    return this.api.getExecutorList();
  }

  public changeTask(data): Observable<ITask> {
    return this.api.changeTask(data);
  }
}
