import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ITask } from '../model/task.interface';
import { IStatus } from '../model/status.interface';
import { IExecutor } from '../model/executor.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private readonly END_POINT = {
    allTasks: '/odata/tasks',
    task: 'Tasks',
    statusList: 'Statuses',
    executorList: 'Users'
  };

  constructor(private http: HttpClient) { }

  public getAllTasks(): Observable<ITask[]> {
    return this.http.get<{ value: ITask[] }>(this.END_POINT.allTasks).pipe(
      map(respone => respone.value)
    );
  }

  public addTask(data): Observable<number> {
    return this.http.post<number>(this.END_POINT.task, data);
  }

  public getTask(id: number): Observable<ITask> {
    return this.http.get<ITask>(`${this.END_POINT.task}/${id}`);
  }

  public getStatusList(): Observable<IStatus[]> {
    return this.http.get<IStatus[]>(this.END_POINT.statusList);
  }

  public getExecutorList(): Observable<IExecutor[]> {
    return this.http.get<IExecutor[]>(this.END_POINT.executorList);
  }

  public changeTask(data): Observable<ITask> {
    return this.http.put<ITask>(this.END_POINT.task, data);
  }
}
