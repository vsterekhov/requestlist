import { HttpInterceptor, HttpRequest, HttpEvent, HttpHandler, HTTP_INTERCEPTORS, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Provider } from '@angular/core';

export class NetworkInterceptor implements HttpInterceptor {
    private readonly tenantguid: string = 'b17252aa-5a08-45d7-9052-e65fd01a81ca';
    private readonly host: string = 'http://intravision-task.test01.intravision.ru';

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (req.url.includes('/odata/tasks')) {
            const params = new HttpParams().set('tenantguid', this.tenantguid);
            const clone = req.clone({
                url: this.host + req.url,
                params
            });

            return next.handle(clone);

        } else {
            const clone = req.clone({
                url: `${this.host}/api/${this.tenantguid}/${req.url}`
            });

            return next.handle(clone);
        }
    }
}

export const INTERCEPTOR_PROVIDER: Provider = {
    provide: HTTP_INTERCEPTORS,
    useClass: NetworkInterceptor,
    multi: true
}