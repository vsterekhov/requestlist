import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { IStatus } from '../model/status.interface';
import { Observable } from 'rxjs';
import { TaskStore } from 'src/app/request/service/task.service';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class StatusResolver implements Resolve<IStatus[]> {
    constructor(private store: TaskStore) {}

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
      ): Observable<IStatus[]> | Promise<IStatus[]> | IStatus[] {
        return this.store.getStatusList();
      }
}