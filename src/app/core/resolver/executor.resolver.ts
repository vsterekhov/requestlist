import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { IExecutor } from '../model/executor.interface';
import { Observable } from 'rxjs';
import { TaskStore } from 'src/app/request/service/task.service';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ExecutorResolver implements Resolve<IExecutor[]> {
    constructor(private store: TaskStore) {}

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
      ): Observable<IExecutor[]> | Promise<IExecutor[]> | IExecutor[] {
        return this.store.getExecutorList();
      }
}
