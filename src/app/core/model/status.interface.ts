export interface IStatus {
    rgb: string;
    id: number;
    name: string;
}