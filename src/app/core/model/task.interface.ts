export interface ITask {
    id: number;
    name: string;
    description: string;
    statusId: number;
    statusName: string;
    statusRgb: string;
    executorId: number;
    executorName: number;
}